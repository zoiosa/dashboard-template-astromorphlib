![alt text](logo.png)

# Dashboard template for Astromorphlib

This repository contains a dashboard template for a morphological analysis
of Fornax cluster. The data was generate using [`astromorph`](https://gitlab.com/joseaher/astromorphlib) package.

[See what it looks like here.](https://joseaher.gitlab.io/fornax-morphology/)

## Documentation

The documentation of astromorphlib  is available at https://astromorphlib.readthedocs.io

---

## Running in your PC

An enhance and faster version of this Dashboard can be used by cloning this repository:

    git clone git@gitlab.com:joseaher/dashboard-template-astromorphlib.git
   OR

    git clone https://gitlab.com/joseaher/dashboard-template-astromorphlib.git

 go to dashboard-template-astromorphlib directory and execute the command:

    bokeh serve --show dashboard


See requirement file to see the versions of
the packages to run the app properly.

## Authors

- Jose Hernandez-Jimenez (joseaher@gmail.com)
- Angela Krabbe                              


**Acknowledgements**

This software was funded partially by Brazilian agency FAPESP,
process number 2021/08920-8.

## Citing

If you use this code for a scientific publication, please
cite [Hernandez-Jimenez & Krabbe et al. (2022)](https://zenodo.org/records/6940848#.YzS4ENXMJH4).
The BibTeX entry for this package is:

```
@software{hernandez_jimenez_2022_6940848,
  author       = {Hernandez-Jimenez, J. A. and
                  Krabbe, A. C.},
  title        = {{Astromorphlib: Python scripts to analyze the
                   morphology of isolated and interacting galaxies}},
  month        = jul,
  year         = 2022,
  publisher    = {Zenodo},
  version      = {0.2},
}
```
