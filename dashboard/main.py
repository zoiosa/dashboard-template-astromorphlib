from astropy.table import Table
import numpy as np
import pandas as pd
from scipy import stats  as  ST

from bokeh.layouts import column, row
from bokeh.models import Select, ColumnDataSource, HoverTool, PreText, TextInput,  Range1d, Whisker

from bokeh.palettes import Spectral6, RdYlGn10, PuOr10, RdYlBu6, RdYlGn6
from bokeh.plotting import curdoc, figure, output_file
from bokeh.sampledata.autompg import autompg_clean as df
from bokeh.transform import factor_cmap, jitter

from bokeh.models import Button

#######################
# Loading the tables  #
#######################

homedir = 'dashboard/'
group1 = 'fornax'

nsa_pairs = Table.read(homedir + 'properties_{}.dat'.format(group1),
                       format='ascii')
print ("\nNumber of object of group {} = {}".format(group1,len(nsa_pairs)))
galaxies_with_flag = len(nsa_pairs[(nsa_pairs['flag_morph']!=0) | (nsa_pairs['flag_sersic']!=0)])
print ("Galaxies with flag of {}: {}".format(group1, galaxies_with_flag))
nsa_pairs = nsa_pairs[(nsa_pairs['flag_morph']==0) & (nsa_pairs['flag_sersic']==0)]
print ("Number of object without flags {} = {}\n".format(group1,len(nsa_pairs)))

df = nsa_pairs
dfp = df.to_pandas()
cor_mat = dfp.corr(method='spearman',) #numeric_only=True)

#################################################
# Definitionf of sizes, colormaps, color ranges #
#################################################

SIZES = list(range(6, 22, 3))
COLORS = PuOr10

N_SIZES = len(SIZES)
N_COLORS = len(COLORS)

#######################
# Photometric models  #
#######################

homed = homedir + 'static/'
urls = []
for i,im in enumerate(df['GAL']):
    url = homed+'fig_'+im+'_{:.0f}kpc_stat.png'.format(df['sizep'][i])
    urls.append(url)
    #print (url)
df['models'] = urls


###########
# Stamps  #
###########

urls = []
for i,im in enumerate(df['GAL']):
    url = homed+'fig_'+im[:-1]+'_{:.0f}kpc.png'.format(df['sizep'][i])
    urls.append(url)
df['stamps'] = urls

#######################################
## Defining the data sets and tooltip #
#######################################

source = ColumnDataSource()


tooltip = """
   <div>
       <div>
       <img src="@models" height="500" width="750"
       style="float: left"
       </img>
       </div>
       <div>
        <img
            src="@stamps" height="450"  width="450"
            style="float: left; margin: 0px 15px 15px 0px;"
            border="2"
        ></img>
      </div>
      <div>
       <span style="font-size: 15px; color: #696; font-weight: bold; ">GAL:</span>
       <span style="font-size: 15px; color: #966;">@gal</span>
       </div>
      <div>
      <div>
       <span style="font-size: 15px; color: #696; font-weight: bold; ">@yvalue:</span>
       <span style="font-size: 15px; color: #966;">@y (y-axis)</span>
       </div>
      <div>

       <span style="font-size: 15px; color: #696; font-weight: bold; ">@xvalue:</span>
       <span style="font-size: 15px; color: #966;">@x (x-axis)</span>
       </div>
      <div>

       <span style="font-size: 15px; color: #696; font-weight: bold; ">@svalue:</span>
       <span style="font-size: 15px; color: #966;">@ss  (Size)</span>
       </div>
      <div>
       <span style="font-size: 15px; color: #696; font-weight: bold; ">@cvalue:</span>
       <span style="font-size: 15px; color: #966;">@cc (Color)</span>
       </div>
     <div>
      <span style="font-size: 15px; color: #696; font-weight: bold; ">flag_model:</span>
      <span style="font-size: 15px; color: #966;">@f_mod</span>
      <span style="font-size: 15px; color: #696; font-weight: bold; ">flag_par:</span>
      <span style="font-size: 15px; color: #966;">@f_par</span>
      </div>
    </div>
    """

def create_figure():


    p = figure(height=800, width=1200,margin=(200,0,0,0),
               tools='pan,box_zoom,wheel_zoom,hover,box_select,save,reset',
               tooltips=tooltip, y_axis_type = y_scale.value,
               x_axis_type = x_scale.value)

    p.xaxis.axis_label = x.value
    p.yaxis.axis_label = y.value
    p.title.text ="{} vs {}".format(x.value, y.value)

    groups = pd.qcut(df[size.value].astype(float), N_SIZES, duplicates='drop')
    sz = [SIZES[xx] for xx in groups.codes]

    groups = pd.qcut(df[color.value].astype(float), 10, duplicates='drop')
    c = [COLORS[xx] for xx in groups.codes]

    xvalue = np.copy(df[x.value].astype(str))
    xvalue[:] = x.value
    df['xvalue'] = xvalue

    yvalue = np.copy(df[y.value].astype(str))
    yvalue[:] = y.value
    df['yvalue'] = yvalue

    cvalue = np.copy(df[color.value].astype(str))
    cvalue[:] = color.value
    df['cvalue'] = cvalue

    svalue = np.copy(df[size.value].astype(str))
    svalue[:] = size.value
    df['svalue'] = svalue

    source.data=dict(
        x=df[x.value].astype(float),
        y=df[y.value].astype(float),
        color=c,
        size=sz,
        cc = df[color.value].astype(float),
        ss = df[size.value].astype(float),
        gal = df['GAL'],
        models = df['models'],
        stamps = df['stamps'],
        xvalue = df['xvalue'],
        yvalue = df['yvalue'],
        cvalue = df['cvalue'],
        svalue = df['svalue'],
        f_par = df['flag_morph'],
        f_mod = df['flag_sersic'],
        )

    p.circle(x='x', y='y',  color='color', size='size', source=source,
             line_color="white", alpha=0.4)

    if x.value=='A':
        p.x_range = Range1d(-0.2,0.8)
    if y.value=='A':
        p.y_range = Range1d(-0.2,0.8)

    if y.value=='Gini' and x.value=='M20':

        xl = np.linspace(0,-3,100)
        yl = -0.14*xl+0.33
        p.line(xl,yl)

        xl=np.linspace(-1.68,-3,100)
        yl = 0.14*xl+0.80
        p.line(xl,yl, line_dash='dashed')

        p.x_range.flipped = True

    if y.value=='C' and x.value=='A':

        b = -9.5
        y0 = 4.85
        xl = np.linspace(0, 0.35, 50)
        yl = b*xl + y0
        p.line(xl,yl, color='black', line_width=2)

    if y.value=='Ser_n' and x.value=='A':

        b = -4.75
        y0 = 1.73
        xl = np.linspace(0, 0.35, 50)
        yl = b*xl + y0
        p.line(xl,yl, color='black', line_width=2)

    if y.value=='F(G_M20)' and x.value=='A':

        b = -4.75
        y0 = 0.73
        xl = np.linspace(0, 0.45, 50)
        yl = b*xl + y0
        p.line(xl,yl, color='black', line_width=2)

    return p


def update(attr, old, new):
    layout.children[1] = create_figure()

    dft = dfp
    selected = source.selected.indices

    if selected:
        dft = dfp.iloc[selected, :]

    cor=ST.spearmanr(dft[x.value],dft[y.value], nan_policy='omit')

    stats.text = 'Corr {} vs {} : {:.2f}\n p_value : {}'.format(x.value,
                  y.value, cor[0], cor[1])
    stats_x.text = str(cor_mat[x.value][(cor_mat[x.value]>0.2)  | (cor_mat[x.value]<-0.2)])
    stats_y.text = str(cor_mat[y.value][(cor_mat[y.value]>0.2)  | (cor_mat[y.value]<-0.2)])


list_opts  = [ 'Gini','M20','C','A','S','As', 'Ao', 'F(G_M20)',
               'S(G_M20)', 'r20', 'r80', 'Ser_R', 'Ser_n', 'z']


x = Select(title='X-Axis', value='M20', options=list_opts)
x.on_change('value', update)

x_scale = Select(title='X-Axis-Scale', value='auto', options=(['auto','log']))
x_scale.on_change('value', update)

y = Select(title='Y-Axis', value='Gini', options=list_opts)
y.on_change('value', update)

y_scale = Select(title='Y-Axis-Scale', value='auto', options=(['auto','log']))
y_scale.on_change('value', update)

size = Select(title='Size', value='A', options=list_opts)
size.on_change('value', update)

color = Select(title='Color', value='C', options=list_opts)
color.on_change('value', update)


stats = PreText(text='Correlation')
stats_x = PreText(text='Correlation X')
stats_y = PreText(text='Correlation Y')

source.selected.on_change('indices', update)

controls = column(y, x, size, color, x_scale, y_scale,
                  stats, width=300,
                  margin=(200,0,0,0))

layout = row(controls, create_figure(),
             column(stats_x, width=200,margin=(200,0,0,0)),
             column(stats_y,width=200, margin=(200,0,0,0)))

curdoc().add_root(layout)
